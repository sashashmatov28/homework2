import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class СalculationsTest {

    private final static Points POINTS = new Points(new int[]{2, 3, 1, 2, 4, 5, 1 - 3, 2, -7}, new int[]{1, -2, 4, 3, 5, -1, -5, -2, 1, -2});
    private final static Circle CIRCLE = new Circle(0, 0, 4);


    @Test
    void shouldCalculationTest() {

        Сalculations сalculations = new Сalculations(POINTS,CIRCLE);


        assertEquals(16,сalculations.searchRadius());

    }


}