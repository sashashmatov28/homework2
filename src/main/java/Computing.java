import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Computing {


    private final UserUi userUi;


    public void run() {
        Circle circle = userUi.readCircle();
        Points points = userUi.readPoint();
        Сalculations сalculations = new Сalculations(points, circle);
        сalculations.searchPoints();

    }


}
