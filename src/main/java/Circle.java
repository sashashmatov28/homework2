import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.Scanner;


@AllArgsConstructor
@Getter
public class Circle {

    private int x;
    private int y;
    private int radius;


}
