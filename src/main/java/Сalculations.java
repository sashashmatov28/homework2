import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Сalculations {
    private Points points;
    private Circle circle;

    public void searchPoints() {
        for (int i = 0; i < 10; i++) {
            if (((Math.pow((circle.getX() - points.getPointX(i)), 2)) + (Math.pow(circle.getY() - points.getPointY(i), 2))) <= searchRadius()) {
                System.out.println("Point " + points.getPointX(i) + " " + points.getPointY(i) + " lies in a circle");
            }

        }
    }

    public int searchRadius() {
        return (int) Math.pow(circle.getRadius(), 2);
    }

}
