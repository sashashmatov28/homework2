import lombok.AllArgsConstructor;

@AllArgsConstructor

public class Points {
    private final int[] pointX;
    private final int[] pointY;


    public int getPointX(int i) {
        return pointX[i];
    }

    public int getPointY(int i) {
        return pointY[i];
    }
}
