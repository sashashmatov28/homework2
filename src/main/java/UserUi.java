import java.io.PrintStream;
import java.util.Scanner;

public class UserUi {
    private final Scanner scanner;
    private final PrintStream out;

    public UserUi(Scanner scanner, PrintStream out) {
        this.scanner = scanner;
        this.out = out;
    }

    public Circle readCircle() {
        out.println("Enter the radius and coordinates of the center of the circle");
        System.out.print("x = ");
        int x = scanner.nextInt();
        out.print("y = ");
        int y = scanner.nextInt();
        out.print("radius = ");
        int radius = scanner.nextInt();
        scanner.nextLine();
        return new Circle(x, y, radius);
    }

    public Points readPoint() {
        out.println("Enter 10 points");
        int[] pointX = new int[10];
        int[] pointY = new int[10];
        for (int i = 0; i < 10; i++) {
            out.print("x" + (i + 1) + " = ");
            int x = scanner.nextInt();
            out.print("y" + (i + 1) + " = ");
            int y = scanner.nextInt();
            out.println();
            scanner.nextLine();
            pointX[i] = x;
            pointY[i] = y;
        }
        return new Points(pointX, pointY);
    }
}
